<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"> 
  <xsl:strip-space elements="*"/>
  <xsl:output method="text" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <xsl:text>\documentclass[2-similaj]{subfiles}&#10;</xsl:text>
    <xsl:text>\begin{document}&#10;</xsl:text>
    <xsl:text>\begin{longtable}{m{0.08\textwidth} m{0.77\textwidth} m{0.15\textwidth}}&#10;</xsl:text>
    <xsl:text> № &#38; условие &#38; отговор \\ &#10;</xsl:text>
    <xsl:text>\hline</xsl:text>
    <xsl:for-each select="ekzamenoj/ekzameno/problemoj/problemo/similaj/problemo/latex">
      <xsl:text>\Circled{</xsl:text>
      <xsl:value-of select="position()" />
      <xsl:text>}</xsl:text>
      <xsl:text> &#38; </xsl:text>
      <xsl:value-of select="kondiĉo"/>
      <xsl:text> &#38; </xsl:text>
      <xsl:value-of select="respondo"/>
      <xsl:text> \\&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>\end{longtable}&#10;</xsl:text>
    <xsl:text>\end{document}</xsl:text>
  </xsl:template>
</xsl:stylesheet>
