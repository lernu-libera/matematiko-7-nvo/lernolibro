<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"> 
  <xsl:strip-space elements="*"/>
  <xsl:output method="text" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <xsl:text>\documentclass[precipa]{subfiles}&#10;</xsl:text>
    <xsl:text>\begin{document}&#10;</xsl:text>
    <xsl:text>\chapter{Подобни на вече даваните предишни години задачи}&#10;</xsl:text>
    <xsl:for-each select="ekzamenoj/ekzameno">
      <xsl:text>&#10;</xsl:text>
      <xsl:text>\section{Изпит </xsl:text>
      <xsl:value-of select="dato"/>
      <xsl:text>}&#10;</xsl:text>
      <xsl:for-each select="problemoj/problemo">
        <xsl:text>\subsection{Задача </xsl:text>
        <xsl:value-of select="numero"/>
        <xsl:text>}&#10;</xsl:text>
        <xsl:text>\subfile{</xsl:text>
        <xsl:value-of select="substring(similaj, string-length(similaj)-24, 21)"/>
        <xsl:text>}&#10;</xsl:text>
        <xsl:text>\newpage&#10;</xsl:text>
      </xsl:for-each>
    </xsl:for-each>
    <xsl:text>\end{document}</xsl:text>
  </xsl:template>
</xsl:stylesheet>
