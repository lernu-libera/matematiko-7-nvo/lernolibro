for $ekzameno in doc("https://lernu-libera.gitlab.io/matematiko-7-nvo/generita-problemoj/similaj.xml")/ekzamenoj/ekzameno
return
<ekzameno>
  <section>Изпит {data($ekzameno/dato)}</section>{
    for $problemo in $ekzameno/problemoj/problemo
    return
    <problemo>
      <subsection>Задача {data($problemo/numero)}</subsection>{
        let $similajurl := data($problemo/similaj),
        $similaj := doc($similajurl)
        return <similaj>{
          for $latex  at $index in $similaj//latex
          let $kondiĉo := $latex/kondiĉo,
          $respondo := $latex/respondo
          return 
          <simila>
            <index>{$index}</index>
            {$kondiĉo}
            {$respondo}
          </simila>
        }</similaj> 
      }
    </problemo>
  }
</ekzameno>
 
