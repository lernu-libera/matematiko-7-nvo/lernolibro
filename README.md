# Дипломна работа към Тракийския университет
Дова е домът на кода, с който се изгражда дипломната ми работа за учител по информатика и информационни технологии към Тракийския университет в Стара Загора.
А [това е вече изграденият ѝ образ](https://gnx.gitlab.io/tru-tezo/tezo.pdf) като PDF.

Shield: [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
